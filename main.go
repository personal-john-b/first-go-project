package main

import (
	"database/sql"
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

func main() {
	var err error
	db, err := sql.Open("postgres", "user=postgres password=postgres dbname=first_go_project sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping();err != nil {
		panic(err)
	}else{
		fmt.Println("DD Connected...")
	}

	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	// Root route => handler
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!\n")
	})



	type Employee struct {
		Id     int `json:"id"`
		Name   string `json:"name"`
		Salary int `json:"salary"`
		Age    int `json:"age"`
	}

	type Employees struct {
		Employees []Employee `json:"employees"`
	}

	e.POST("/employee", func(c echo.Context) error {
		u := new(Employee)
		if err := c.Bind(u);err != nil {
			return err
		}

		sqlStatement := "INSERT INTO employee (name, salary, age) values ($1,$2,$3) returning id"
		err := db.QueryRow(sqlStatement, u.Name, u.Salary, u.Age).Scan(&u.Id)

		if err != nil {
			fmt.Println(err)
		}else{
			fmt.Println(u)
			return c.JSON(http.StatusCreated,u)
		}
		return c.String(http.StatusCreated, "ok")
	})

	e.PUT("/employee/:id", func(c echo.Context) error {
		id := c.Param("id")
		u := new(Employee)
		if err := c.Bind(u); err != nil {
			panic(err)
			return err
		}

		sqlStatement := "UPDATE employee  SET name=$1,salary=$2,age=$3 WHERE id=$4"
		res, err := db.Query(sqlStatement, u.Name, u.Salary, u.Age, id)
		if err != nil {
			fmt.Println(err)
			//return c.JSON(http.StatusCreated, u);
		} else {
			fmt.Println(res)
			return c.JSON(http.StatusOK, u)
		}
		return c.String(http.StatusOK, id)
	})

	e.DELETE("/employee/:id", func(c echo.Context) error {
		id := c.Param("id")
		sqlStatement := "DELETE FROM employee WHERE id = $1"
		res, err := db.Query(sqlStatement, id)
		if err != nil {
			fmt.Println(err)
			//return c.JSON(http.StatusCreated, u);
		} else {
			fmt.Println(res)
			return c.JSON(http.StatusOK, "Deleted")
		}
		return c.String(http.StatusOK, id+"Deleted")
	})

	e.GET("/employee", func(c echo.Context) error {
		sqlStatement := "SELECT id, name, salary, age FROM employee order by id"
		rows, err := db.Query(sqlStatement)
		if err != nil {
			fmt.Println(err)
			//return c.JSON(http.StatusCreated, u);
		}
		defer rows.Close()
		result := Employees{}

		for rows.Next() {
			employee := Employee{}
			err2 := rows.Scan(&employee.Id, &employee.Name, &employee.Salary, &employee.Age)
			// Exit if we get an error
			if err2 != nil {
				return err2
			}
			result.Employees = append(result.Employees, employee)
		}
		return c.JSON(http.StatusCreated, result)

		//return c.String(http.StatusOK, "ok")
	})

	// Run Server
	e.Logger.Fatal(e.Start(":8000"))
}